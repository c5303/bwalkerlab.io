---
title: HTB-Socket
---

## Socket WriteUp

<center>![](/images/WriteUps/Socket/header.png)</center>

<style>

p {
	color: #A8D8B9;
	text-align: center;
	font-size: 1.2em;
	width: 50%;
	margin: 0 auto;
	margin-bottom: 20px;
}

img {
    display: block;
    margin: 0 auto;
}

.image-container {
  max-width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
}

.responsive-image {
  max-width: 100%;
  height: auto;

</style>


<h2 id="recon" style="color: greenyellow;"><u>Recon/Scanning</u></h2>
<!-- content for Recon/Scanning goes here -->

<div class="image-container">
  <img src="/images/WriteUps/Socket/scan.png" alt="BroScience" class="responsive-image">
</div>

<p >
	Adding to <span style="color: greenyellow;">/etc/hosts</span>...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/host.png" alt="BroScience" class="responsive-image">
</div>

<p >
	Checking out the site on 80, we can see Text to QR code page...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/site.png" alt="BroScience" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Socket/flask.png" alt="BroScience" class="responsive-image">
</div>

<p >
	Intercepting we can see Werkzeug/2.1.2 from the Server Header...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/server.png" alt="BroScience" class="responsive-image">
</div>

<p >
	We can download the QReader binary, I used Ghidra to check it out...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/binary.png" alt="BroScience" class="responsive-image">
</div>

<p style='text-align: left;'>
	After digging a bit, I realized this could probably be a rabbit hole, but then I seen PyInstaller which triggered the remembrance of a <a href="https://github.com/extremecoders-re/pyinstxtractor" target="_blank">tool</a> that could extract the contents of a PyInstaller generated file. You can find that info digging with Ghidra...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/pyInstaller.png" alt="BroScience" class="responsive-image">
</div>

<p >
	lets extract...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/pyExtractor.png" alt="BroScience" class="responsive-image">
</div>

<p style='text-align: left;'>
	We will need <span style="color: greenyellow;">uncompyle6</span> which is a library that translates Python bytecode back into its equivalent Python source code. It was a pain in the arse because I use 3.11 and this lib only works under 3.8! If you are using hArch you can use altinstall<br>
</p>

<div class="code-snippet">

```bash
#Extract
tar -xvf ~/path/to/python

#Configure
cd extracted/content && ./configure

#Make
make

#Install alongside
sudo make altinstall
```

</div>

<p>
	We can easily attain this with a Virtual Environment! 
</p>	

<div class="image-container">
  <img src="/images/WriteUps/Socket/pythonVersions.png" alt="BroScience" class="responsive-image">
</div>

<p >
	Looking at qreader.py, there is a function that jumps out...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/vuln.png" alt="BroScience" class="responsive-image">
</div>

<p style='text-align: left;'>
	Why does this look interesting? The way the response is dumping the data 'version': VERSION, there does not seem to be any sanitation happening. In theory, if we can inject without breaking the response, we might get back something worth something.<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/noConnect.png" alt="BroScience" class="responsive-image">
</div>

<p >
	We cannot access it directly, header issues... Lets code WebSockets <3<br>
</p>

<div class='code-snippet'>

```python
import asyncio
import websockets
import json

async def connect_to_websocket():
    uri = "ws://qreader.htb:5789/ws" #FUZZ

    print("WebSocket connection established!")  

    # Start sending and receiving messages
    while True:
        async with websockets.connect(uri) as websocket:
            message = input("Enter a message: ")
            data = {"data": message}
            await websocket.send(json.dumps(data))
            print("Message sent!")

            try:
                response = await websocket.recv()
                response_data = json.loads(response)
                print("Received message:", response_data)
            except websockets.exceptions.ConnectionClosedError as e:
                print("WebSocket connection closed with error:", e)
                break

asyncio.get_event_loop().run_until_complete(connect_to_websocket())
```

</div>

<div class="image-container">
  <img src="/images/WriteUps/Socket/webSock.png" alt="BroScience" class="responsive-image">
</div>

<p >
	This is a basic script to send and recv from a WebSocket.<br>
</p>

<p >
	Now with a few mods, we can play around with the /version endpoint...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/versionCheck.png" alt="BroScience" class="responsive-image">
</div>

<p >
	hmmm.. I wonder if this is vulnerable to SQLi<br>
</p>

<p >
	After a bit of testing and error handling... It is starting too look like it...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/SQL.png" alt="BroScience" class="responsive-image">
</div>

<p >
	Kerpow!
</p>

<h2 id="access" style="color: darkred;"><u>Gaining Access</u></h2>
<!-- content for Gaining Access goes here -->

<p >
	So we are dealing with a SQL Injection UNION Attack, lets dump something...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/hashFound.png" alt="BroScience" class="responsive-image">
</div>

<p>
	MD5?
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/md5Hash.png" alt="BroScience" class="responsive-image">
</div>

<p>
	Cracking...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/password.png" alt="BroScience" class="responsive-image">
</div>

<p style='text-align: left;'>
	Crackstation was the quicker option here... but I can't do anything with this because I have no username. I probably could throw a list of users in Hydra but that is no fun! Back to enumeration... 
</p>

<p>
	It took a HOT minute, but finally got something! Lets recap...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/SQLi.png" alt="BroScience" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Socket/usernames.png" alt="BroScience" class="responsive-image">
</div>

<p style='text-align: left;'>
	We can see a few usernames, I tried Mike, then json (which I thought maybe typo so Jason) all with no success. My AD name creation mind took over and I took Thomas Keller and whipped up a file with a few usernames that could possibly work.
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/wordlist.png" alt="BroScience" class="responsive-image">
</div>

<p>
	You can quickly test SSH access against a list in your terminal ;)
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/user.png" alt="BroScience" class="responsive-image">
</div>

<p>
	We made it, and got the user flag...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/userFlag.png" alt="BroScience" class="responsive-image">
</div>


<h2 id="privilege" style="color: lightgoldenrodyellow;"><u>Privilege Escalation</u></h2>
<!-- content for Privilege Escalation goes here -->

<p>
	As always, lets get a feel of where we are and what we can do...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/sudoRights.png" alt="BroScience" class="responsive-image">
</div>

<p>
	Investigating this build-installer shell file...
</p>

<p>
  <i><u><span style="color: #15e9e9;">/usr/local/sbin/build-installer.sh</span></u></i>
</p>

<div class='code-snippet'>

```bash
#!/bin/bash
if [ $# -ne 2 ] && [[ $1 != 'cleanup' ]]; then
  /usr/bin/echo "No enough arguments supplied"
  exit 1;
fi

action=$1
name=$2
ext=$(/usr/bin/echo $2 |/usr/bin/awk -F'.' '{ print $(NF) }')

if [[ -L $name ]];then
  /usr/bin/echo 'Symlinks are not allowed'
  exit 1;
fi

if [[ $action == 'build' ]]; then
  if [[ $ext == 'spec' ]] ; then
    /usr/bin/rm -r /opt/shared/build /opt/shared/dist 2>/dev/null
    /home/svc/.local/bin/pyinstaller $name
    /usr/bin/mv ./dist ./build /opt/shared
  else
    echo "Invalid file format"
    exit 1;
  fi
elif [[ $action == 'make' ]]; then
  if [[ $ext == 'py' ]] ; then
    /usr/bin/rm -r /opt/shared/build /opt/shared/dist 2>/dev/null
    /root/.local/bin/pyinstaller -F --name "qreader" $name --specpath /tmp
   /usr/bin/mv ./dist ./build /opt/shared
  else
    echo "Invalid file format"
    exit 1;
  fi
elif [[ $action == 'cleanup' ]]; then
  /usr/bin/rm -r ./build ./dist 2>/dev/null
  /usr/bin/rm -r /opt/shared/build /opt/shared/dist 2>/dev/null
  /usr/bin/rm /tmp/qreader* 2>/dev/null
else
  /usr/bin/echo 'Invalid action'
  exit 1;
fi
```

</div>

<p>
	Interesting... we might be able to work with this 
</p>

<p style='text-align: left;'>
If the 'action' is 'build', it checks if the file extension is 'spec'. If it is, it removes the directories '/opt/shared/build' and '/opt/shared/dist' if they exist, and then uses the 'pyinstaller' command to build the specified file. Finally, it moves the generated 'dist' and 'build' directories to '/opt/shared'
</p>

<p>
	What would happen if we created a malicous .spec file ? 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/priv.png" alt="BroScience" class="responsive-image">
</div>

<p>
	Now we need to build this... 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/privEsc.png" alt="BroScience" class="responsive-image">
</div>


<h2 id="wrapup" style="color: royalblue;"><u>Wrapping Up</u></h2>
<!-- content for Wrapping Up goes here -->

<p style='text-align: left;'>
 "Socket" from Hackthebox presents a thrilling and challenging experience, showcasing the intricacies of a <span style='color: orangered;'>Medium</span> level machine. The utilization of websockets adds an interesting layer of complexity, prompting concerns about potential vulnerabilities if not implemented securely. Engaging in ethical hacking practices, such as WebSocket testing, proves invaluable in uncovering weaknesses in data transmission and authentication mechanisms, ultimately fortifying the overall security of the system.<br><br>
Moreover, the discovery of a vulnerability in the binary file emphasizes the criticality of adhering to secure software development practices. Ethical hackers play a crucial role in this process, employing various techniques such as binary analysis, code review, and fuzz testing to proactively identify and address vulnerabilities early on in the development lifecycle.<br><br>
By immersing ourselves in challenges like "Socket," we not only hone our technical skills but also deepen our understanding of the significance of cybersecurity and the imperative to foster a security-first mindset in all our endeavors. As we continue on our ethical hacking journey, each encounter enriches our expertise and resilience in the ever-evolving landscape of cybersecurity. Together, we strive towards a more secure and resilient digital world.
</p>

<div class="image-container">
  <img src="/images/WriteUps/Socket/shoutout.png" alt="BroScience" class="responsive-image">
</div>

<p>
  Amazing challenge by Kavigihan! 
</p>