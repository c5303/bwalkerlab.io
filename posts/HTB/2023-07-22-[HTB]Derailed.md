---
title: HTB-Derailed
---

# Derailed Writeup
<center>![](/images/WriteUps/Derailed/header.png)</center>

<style>

p {
	color: #A8D8B9;
	text-align: center;
	font-size: 1.2em;
	width: 50%;
	margin: 0 auto;
	margin-bottom: 20px;
}

img {
    display: block;
    margin: 0 auto;
}

.image-container {
  max-width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
}

.responsive-image {
  max-width: 100%;
  height: auto;

</style>


<h2 id="recon" style="color: greenyellow;"><u>Recon/Scanning</u></h2>
<!-- content for Recon/Scanning goes here -->

<div class="image-container">
  <img src="/images/WriteUps/Derailed/scan.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/hosts.png" class="responsive-image">
</div>

<p>
	Enumerating 3000...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/mainPage.png" class="responsive-image">
</div>

<p>
	Looks to be a app too add notes, we can post without signing up as Guest X_x
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/testNote.png" class="responsive-image">
</div>

<p>
	Directory Enumeration from behind the scenes... 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/smallDirBrute.png" class="responsive-image">
</div>

<p>
	Remember <span style="color: yellowgreen;">/administration</span>, I am sure it will come in handy...
</p>


<p style="text-align: left;">
	We can try <span style="color: yellow;">unauthenticated</span> testing. Ill play around with this to get a better feel for the app then create an account! Its important to always test both approaches!
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/rails.png" class="responsive-image">
</div>

<p>
	A Ruby on Rails app? Derailed... makes sense
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/authNote.png" class="responsive-image">
</div>

<p>
	After authenticating, our Author name changed! Curious
</p>

<p>
	CSRF protection, can't stop XSS so nothing we still cannot automate through ;) 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/meme.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/reportFeature.png" class="responsive-image">
</div>

<p style="text-align: left;">
	There is a report feature that allows us to report our clipnote, if we do we see "<span style="color: forestgreen;">The note has been reported. Our admins will soon have a look at it</span>". Very Interesting... If we have Admin interaction, we have a good attack vector.<br><br>It is worth noting that our Cookie has the HttpOnly flag set. Therefore this means we cannot outright directly steal any cookies. We are going to have to go to insane measures to pop it
</p>

<p style="text-align: left;">
	None of these wordlists are picking up anything so I created a custom wordlist against a <a href="https://www.scaler.com/topics/rails-directory-structure/" target='_blank'>site</a> that talks about the Directory structure! We can brute force easily enough as each directory that does not exist is a 404 and gives "Not Found". We can create a script that recursively checks to attempt to drill down.
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/railBruter.png" class="responsive-image">
</div>

<p>
	Nice! So we are dealing with Ruby on Rails!  
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/redirected.png" class="responsive-image">
</div>

<p>
	We were redirected too <span style="color: yellowgreen;">/routes</span> 
</p>

<p style="text-align: left;">
	Playing around, I found XSS but I don't think we can achieve anything through this current page because we have no other interaction or at least nothing I was able to find.
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/CVE_Found.png" class="responsive-image">
</div>

<p>
	A little later the script found <span style="color: yellowgreen;">/rails/info/properties</span> 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/railsProp.png" class="responsive-image">
</div>

<p>
	Diving around the internet, I landed on <a href="https://groups.google.com/g/rubyonrails-security/c/ce9PhUANQ6s?pli=1" target="_blank">this!</a>  
</p>

<p style="text-align: left;">
"certain configurations of Rails::Html::Sanitizer may allow an attacker to inject content if the application developer has overridden the sanitizer's allowed tags to allow both `select` and `style` elements.Code is only impacted if allowed tags are being overridden."
</p>

<p>
	Working back to the author field (our username ;), XSS is exploited...</span> 
</p>

<p>
	We need to overflow for the exploit to work, we can recv a callback</span> 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/charLimit.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/XSS_Overflow.png" class="responsive-image">
</div>

<p>
	I can say that I have never seen a overflow into XSS... this is pretty damn cool</span> 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/callbackOne.png" class="responsive-image">
</div>

<p>
	So now what? I am thinking CSRF... if this works, we can grab the admin page
</p>

<p>
	Why do it in 20 minutes manually, when you can spend hours to automate it ;) 
</p>

<p style="text-align: left;">
	This was tricky, for this to work, we have to encode our payload as well as use onerror. I tried a few but ASCII ended up working. We will utilize a JS payload to get a callback. For example, this is how I automated my malicious user creation to get our admin callback...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/payload1.png" class="responsive-image">
</div>

<p>
	This is a simple callback, the whole script on <a href="https://book.hacktricks.xyz/pentesting-web/xss-cross-site-scripting" target="_blank">hacktricks</a> is something well need
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/username.png" class="responsive-image">
</div>

<p style="text-align: left;">
	For our Char encoding, I just created a seperate function that will handle the encoding as well as wrapping our payload. Who doesn't love to nestle JS into their Python ;)  
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/encoding.png" class="responsive-image">
</div>

<p>
	Our new payload to steal the page 
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/payload2.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/admin_callback.png" class="responsive-image">
</div>

<p>
	Told you I was lazy, but now I can easily test payloads
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/adminCall.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/keepTrying.png" class="responsive-image">
</div>

<p>
	Never give into despair... The key was to make sure the admin triggers, not me</span> 
</p>

<p>
	You should see the various output, base64 -d to reveal the admin page
</p>

<p>
	There is a reports form that may be injectible, the value param...
</p>

<div class='code-snippet'>

```html
<h3>Reports</h3>
      <form method="post" action="/administration/reports">
        <input type="hidden" name="authenticity_token" id="authenticity_token" value="<auth_token>" autocomplete="off" />
        <input type="text" class="form-control" name="report_log" value="report_14_12_2022.log" hidden>
        <label class="pt-4"> 14.12.2022</label>
        <button name="button" type="submit">
          <i class="fas fa-download me-2"></i>
          Download
        </button>
      </form>
```

</div>


<p>
	Awesome! We can utilize all the information to gain access 
</p>

<p>
	If you are not following along, I highly recommend my <a href='/codeups' target='_blank'>CodeUps</a>
</p>


<h2 id="access" style="color: darkred;"><u>Gaining Access</u></h2>
<!-- content for Gaining Access goes here -->

<p style="text-align: left;">
	After conducting our tests, we discovered the potential for chaining XSS -> CSRF vulnerabilities. This occurs due to an interaction with the admin system when reporting our clipnote, which can lead to the compromise of the administrative session and provide unauthorized access to the administrative page.
</p>

<p style="text-align: left;">
	We observed by accessing the /administration page, we noticed a possible injection vector. The final payload with a few tweaks should catch us our shell.
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/payload3.png" class="responsive-image">
</div>

<p>
	Automated user exploit... Damn I love automation! Did we get the flag?
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/shellCaught.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/userFound.png" class="responsive-image">
</div>

<p>
	A small recap
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/userPOC.png" class="responsive-image">
</div>


<h2 id="privilege" style="color: lightgoldenrodyellow;"><u>Privilege Escalation</u></h2>
<!-- content for Privilege Escalation goes here -->

<p>
	We made it! That was exhilerating!
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/clear.png" class="responsive-image">
</div>


<p>
	With no creds, Ill look at /opt as well as listening ports...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/opt.png" class="responsive-image">
</div>

<p>
	No luck here...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/listeningPorts.png" class="responsive-image">
</div>

<p>
	Quite a few interesting ports listening... going to poke around more
</p>


<div class="image-container">
  <img src="/images/WriteUps/Derailed/find-db.png" class="responsive-image">
</div>

<p>
	Inside this sqlite3 db I found a hash for toby and alice... litte too easy
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/weirdHash.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/crackedHash.png" class="responsive-image">
</div>


<p>
	The password works for openmediavault-webgui aka Toby... 
</p>

<p>
	Enum Enum Enum 
</p>

<p>
	As Toby, I got a sense to look at this openmediavault...  
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/mediaVault.png" class="responsive-image">
</div>

<p>
	I see we have a config file that we can edit, looking through it...   
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/configFile.png" class="responsive-image">
</div>

<p>
	We may be able to do some SSH trickery but I am unsure, need to delve into OMV  
</p>

<p>
	For smoother enumeration and editing, add SSH keys and get SSH access
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/configUsers.png" class="responsive-image">
</div>

<p>
	A good read to study up on the <a href="https://openmediavault-docs.readthedocs.io/en/latest/development/internaltools.html" target="_blank">OMW Internal Tools</a>
</p>

<p>
	 So we we can edit this, but what can we do...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/confdb.png" class="responsive-image">
</div>

<p>
	 hmmmm... I wonder
</p>

<p>
	 We may be able to <a href="https://forum.openmediavault.org/index.php?thread/7822-guide-enable-ssh-with-public-key-authentication-securing-remote-webui-access-to/">add SSH keys</a> for root! Only way to know is to test
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/gen_key.png" class="responsive-image">
</div>

<p>
	 Generate a key, replace the test user with root and add our new sshpubkey...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/sshExploit.png" class="responsive-image">
</div>

<p style="text-align: left;">
	 A sure fire way to know this is working is we should see our new key in the confdbadm output
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/errors.png" class="responsive-image">
</div>

<p>
	 Its screaming about the format of the key...
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/fixedError.png" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/exploitWorked.png" class="responsive-image">
</div>

<p>
	 ssh root@derailed.htb
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/rooted.png" class="responsive-image">
</div>

<p>
	 Rooted! Victory Roar!!!! 
</p>


<h2 id="wrapup" style="color: royalblue;"><u>Wrapping Up</u></h2>
<!-- content for Wrapping Up goes here -->

<p style="text-align: left;">
	 With a patient and persistent approach, I eagerly embraced the challenge of the <span style="color: darkred;">Insane</span> box, fueled by my passion for automation. The dynamic nature of the site presented anticipated obstacles, urging me to deftly manipulate tokens and sessions to ensure it all worked properly. 
</p>

<p style="text-align: left;">
	 XSS was found in the username field of our registered user! An unseen approach to overflow into XSS was very interesting. A curious mind would think to chain XSS into something, in this case the Administrators cookie was not obtainable so why not view what we can't see.   
</p>

<p style="text-align: left;">
	 Viewing the admin page pointed us to an RCE through the report form that we had to chain to execute. XSS -> CSRF -> RCE. This was serious work! This granted us a shell as rails.   
</p>

<p style="text-align: left;">
The critical vulnerability lies in the ownership permissions of the config.xml file, which grants us the ability to modify it at will. Consequently, this empowers us to enable SSH access for any user on the machine, using a public key of our preference. By editing the config file within the system, we can effortlessly exploit this security loophole.
</p>

<div class="image-container">
  <img src="/images/WriteUps/Derailed/shoutout.png" class="responsive-image">
</div>

<p>
Shoutout to both of these creators! Perfect box!  
</p>