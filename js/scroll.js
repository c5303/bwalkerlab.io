let prevScrollPos = window.pageYOffset;

window.onscroll = function() {
  const currentScrollPos = window.pageYOffset;
  const sidenav = document.getElementById("sidenav");

  if (prevScrollPos > currentScrollPos) {
    // Scrolling up, show the sidenav
    sidenav.classList.remove("is-scrolling");
  } else {
    // Scrolling down, hide the sidenav with the auto-hide animation
    sidenav.classList.add("is-scrolling");
  }

  prevScrollPos = currentScrollPos;
};