---
title: About
---

.. raw:: html

   <style>
   .rounded-image {
     border-radius: 50%;
     width: 200px;
     height: 200px;
     object-fit: cover;
   }

   </style>

Father | Veteran | Ethical-Hacker | Snowboarder


.. container::
   :class: flip-container

   .. container::
      :class: flipper

      .. container::
         :class: front

         .. image:: /images/me.jpg
            :alt: Meee
            :align: center
            :class: rounded-image

      .. container::
         :class: back

.. raw:: html

   <div style="display: flex; justify-content: center; margin-top:10px;">
     <div style="color: white; padding: 2px; width: 50%;">
       <p style="text-align: center; font-size: 1.2em;">
         Hello there, my name is Brandon and this is my corner in CyberSpace
      </p>
     </div>
   </div>

      <div style="display: flex; justify-content: center; margin-top:10px;">
     <div style="color: white; padding: 2px; width: 50%;">
       <p style="text-align: left; font-size: 1.2em;">
         I am currently an Automation Engineer for a MSSP. Cybersecurity spearheads everything I do in IT and I hope to one day become a Penetration Tester or be a member of a Red/Purple Team. Below are my Certifications!
       </p>
     </div>
   </div>


  <style>
    .certifications {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;

    }
    
    .certification {
      margin: 10px;
      padding: 10px;
      border: 2px solid #000;
      border-radius: 5px;
      text-align: center;
      width: 200px;
      background-size: cover;
      background-position: center;
      background-repeat: no-repeat;
      background-color: #0076C0;
    }
    
    .certification-title {
      font-size: 18px;
      font-weight: bold;
    }
  </style>
  <body>
    <div class="certifications">
      <div class="certification" style="background-image: url('/images/linPlus.pdf');">
        <h2 class="certification-title"><a href="/images/linPlus.pdf" target="_blank">Linux+</a></h2>
      </div>
      <div class="certification" style="background-image: url('/images/netPlus.pdf');">
        <h2 class="certification-title"><a href="/images/netPlus.pdf" target="_blank">Network+</a></h2>
      </div>
      <div class="certification" style="background-image: url('/images/secPlus.pdf');">
        <h2 class="certification-title"><a href="/images/secPlus.pdf" target="_blank">Security+</a></h2>
      </div>
      <div class="certification" style="background-image: url('/images/pentestPlus.pdf');">
        <h2 class="certification-title"><a href="/images/pentestPlus.pdf" target="_blank">Pentest+</a></h2>
      </div>
    </div>


    <div class="discord-container">
        <h3>Join My Discord Server</h3>
        <p style="text-align: left;">Join the Chaotic Hacking Network to talk too fellow hackers! We welcome any like-minded professional. We are very active and are growing steadily!</p>
        <a href="https://discord.gg/DSZjBGynM" class="discord-button" target="_blank">Join and Hack the Universe!</a>
    </div>

    <div style="display: flex; justify-content: center; margin-top:10px;">
     <div style="color: white; padding: 2px; width: 50%;">
       <p style="text-align: left; font-size: 1.2em;">
         If you find any discrepancies or issues, please reach out to me! I am human, but I want the information to be correct. I have a weird layout, I know! I am determining which default template I want to run with.
       </p>
     </div>
   </div>
  </body>




                                                