--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
{- 
    Data.Monoid (mappend) brings the 'mappend' function into scope
    Data.Monoid is a module that provides a set of types and functions
    for working with Monoids (algebraic stuctures that combine values)
    Mappend is a method of the 'Monoid' type class which provdes a way
    to combine to values of the same type into a single value
 -}
import           Data.Monoid (mappend)
import           Hakyll
import           Data.List (isSuffixOf)
import           Data.Maybe (fromMaybe)
import           System.FilePath
import           Control.Monad.IO.Class (liftIO)
import           Hakyll.Web.CompressCss (compressCssCompiler)
--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
    -- The route function sets the route where the final file will be
    match ("images/**" .||. "js/*" .||. "css/*") $ do
        -- route idRoute simply means to use the same path that the original file has on the system
        route   idRoute
        -- Functions that process files are named with the “Compiler” prefix and they are applied to the matched files with the compile function
        compile copyFileCompiler


    match ("posts/cyberHive/*") $ do
        route $ cleanRoute
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/wrapper.html" postCtx
            >>= relativizeUrls


    match ("posts/THM/*" .||. "posts/codeUps/*" .||. "posts/HTB/*") $ do
        route $ cleanRoute
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/sideNav.html" postCtx
            >>= relativizeUrls

                       
 
    create ["codeups.html"] $ do
        route cleanRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/codeUps/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "posts"       `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/codeUps.html" archiveCtx
                >>= loadAndApplyTemplate "templates/wrapper.html" archiveCtx
                >>= relativizeUrls
                >>= cleanIndexUrls


    match "about.rst" $ do
        route   $ cleanRoute
        compile $ pandocCompiler
                >>= loadAndApplyTemplate "templates/about.html" defaultContext
                >>= relativizeUrls


    create ["sitemap.xml"] $ do
        route idRoute 
        compile $ do
            -- load and sort the posts
            posts <- recentFirst =<< loadAll ("posts/**")

            -- load individual pages from a list (globs DO NOT work here)
            singlePages <- loadAll (fromList ["writeups.html"])

                           -- mappend the posts and singlePages together
            let pages = posts <> singlePages

                           -- create the `pages` field with the postCtx
                           -- and return the `pages` value for it
                sitemapCtx = 
                    constField "root" root <> -- here
                    listField "pages" postCtx (return pages)

            -- make the item and apply our sitemap template
            makeItem ""
                >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx


    match "writeups.html" $ do
        route cleanRoute
        compile $ getResourceBody
                >>= loadAndApplyTemplate "templates/writeups.html" defaultContext
                >>= relativizeUrls
                >>= cleanIndexUrls


    match "index.html" $ do
        route idRoute
        compile $ getResourceBody
                >>= loadAndApplyTemplate "templates/wrapper.html" defaultContext
                >>= relativizeUrls
                >>= cleanIndexUrls

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------
{- 
postCtx adds our date field to blog postings 
-}
postCtx :: Context String
postCtx =
    constField "root" root       <>
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

{- 
root gives our static value for sitemap.xml
-}
root :: String
root = "https://bwalker.xyz"

{- 
We need to create a custom compiler to 
remove the .html tags at the end of any
url 
-}
cleanRoute :: Routes
cleanRoute = customRoute createIndexRoute
  where
    createIndexRoute ident = takeDirectory p </> takeBaseName p </> "index.html"
                            where p = toFilePath ident

cleanIndexUrls :: Item String -> Compiler (Item String)
cleanIndexUrls = return . fmap (withUrls cleanIndex)

cleanIndexHtmls :: Item String -> Compiler (Item String)
cleanIndexHtmls = return . fmap (replaceAll pattern replacement)
    where
      pattern = "/index.html"
      replacement = const "/"

cleanIndex :: String -> String
cleanIndex url
    | idx `isSuffixOf` url = take (length url - length idx) url
    | otherwise            = url
  where idx = "index.html"

